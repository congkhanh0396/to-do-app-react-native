
import React, { useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Task from '../todoApp/component/Task'
const App = () => {

  const [value, setValue] = useState('');

  const [todos, setTodos] = useState([]);

  const handleAddTodo = () => {
    if (value.length > 0) {
      setTodos([...todos, { text: value, key: Date.now(), checked: false }])
      setValue('')
    }
  }

  const handleDeleteTodo = (id) => {
    setTodos(
      todos.filter((todo) => {
        if (todo.key !== id) return true
      })
    )
  }

  const handleChecked = (id) => {
    setTodos(todos.map((todo) => {
      if (todo.key === id) todo.checked = !todo.checked;
      return todo;
    })
    )
  }

  return (

    <ImageBackground source={require('./images/bg.jpg')} style={styles.container}>
      <Text style={styles.title}>To do App</Text>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          multiline={true}
          onChangeText={value => setValue(value)}
          placeholder={'Write some task here !!!'}
          placeholderTextColor="white"
          value={value}
        />
        <TouchableOpacity onPress={() => handleAddTodo()}>
          <Icon name="feather" size={24} color="white" style={{ marginLeft: 15 }} />
        </TouchableOpacity>
      </View>
      <ScrollView style={{ width: '100%' }}>
        {todos.map((task) => (
          <Task
            text={task.text}
            key={task.key}
            checked={task.checked}
            setChecked={() => handleChecked(task.key)}
            delete={() => handleDeleteTodo(task.key)}
          />
        ))
        }
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    resizeMode: 'cover'
  },
  title: {
    marginTop: '10%',
    fontSize: 30,
    color: 'white',

  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'baseline',
    borderColor: 'rgb(222,222,222)',
    borderBottomWidth: 1,
    paddingRight: 10,
    paddingBottom: 5
  },
  textInput: {
    flex: 1,
    paddingLeft: '5%',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: "white"
  },

});

export default App;
