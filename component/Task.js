import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';


const Task = (props) => {
    return (
        <View style={styles.container}>

            <TouchableOpacity>
                <Icon name={props.checked ? "check" : "square"} size={24} color="white" onPress={props.setChecked} />
            </TouchableOpacity>
            <View>
                <Text style={props.checked ? styles.taskDone : styles.task }>{props.text}</Text>
            </View>
            <TouchableOpacity>
                <Icon name="trash-2" disabled={props.checked ? false : true} size={24} color={props.checked ? "white" : "gray"} onPress={props.delete} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    task: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#FFFAF0',
        padding: 5
    },
    taskDone:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#ddd',
        padding: 5,
        textDecorationLine: 'line-through',
        textDecorationColor: 'white'
    }

});

export default Task;